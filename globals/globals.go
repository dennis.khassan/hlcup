package globals

import (
	"fmt"
	"github.com/jmhodges/levigo"
	"github.com/restream/reindexer"
	"go.uber.org/zap"
	"math"
	"regexp"
	"time"
)

var (
	GlobalTimestamp int32

	Logger *zap.Logger

	TimeZone   = "Europe/Moscow"
	TimeFormat = "2006-01-02 15:04:05.000"

	EmailPattern = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	ReindexerDb         *reindexer.Reindexer
	ExtraDb             *levigo.DB
	LevelDbOptions      *levigo.Options
	LevelDbReadOptions  *levigo.ReadOptions
	LevelDbWriteOptions *levigo.WriteOptions

	//LikedByInitMap      = map[uint32][]uint32{}
	//LikedByInitMapMutex sync.RWMutex

	//CntFilter    = uint64(0)
	//CntGroup     = uint64(0)
	//CntLikes     = uint64(0)
	//CntNew       = uint64(0)
	//CntRecommend = uint64(0)
	//CntSuggest   = uint64(0)
	//CntUpdate    = uint64(0)

	ValidID = regexp.MustCompile(`\(([0-9]+)\)`)

	EmptyAccountsString = []byte("{\"accounts\":[]}")
	EmptyGroupsString   = []byte("{\"groups\":[]}")
	EmptyJsonString     = []byte("{}")

	DataUpdated = false

	StatusesArray   = []string{"свободны", "всё сложно", "заняты"} // sequence is important
	PremiumNowArray = []bool{true, false}                          // sequence is important
)

const (
	AccountsNamespace = "accounts"
	ExtraNamespace    = "extra"
)

func GetDurationStr(duration time.Duration) string {
	return fmt.Sprintf("%d:%02ds", int(duration.Minutes()), int(math.Mod(duration.Seconds(), 60)))
	//if duration.Seconds() < 60 {
	//	return fmt.Sprintf("%ds", int(duration.Seconds()))
	//} else {
	//	return fmt.Sprintf("%dm%02ds", int(duration.Minutes()), int(math.Mod(duration.Seconds(), 60)))
	//}
}

func IncrementString(value string) string {
	ret := []byte(value)
	for i := len(ret) - 1; i >= 0; i-- {
		ret[i]++
		if ret[i] != 0 {
			break
		}
	}
	return string(ret)
}
