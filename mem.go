package main

// #include <unistd.h>
import "C"
import (
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
)

func getMemoryMaxPages() int {
	return int(C.sysconf(C._SC_PHYS_PAGES))
}

func getMemoryPageSize() int {
	return int(C.sysconf(C._SC_PAGE_SIZE))
}

func getClockTicks() int {
	return int(C.sysconf(C._SC_CLK_TCK))
}

func getProcCount() int {
	return int(C.sysconf(C._SC_NPROCESSORS_ONLN))
}

func getUsage() (uint64, float64, float64) {
	stat, err := ioutil.ReadFile("/proc/self/stat")
	if err != nil {
		return 0, 0, 0
	}
	stats := strings.Split(string(stat), " ")
	if len(stats) < 24 {
		return 0, 0, 0
	}
	memory, err := strconv.ParseUint(stats[23], 10, 64)
	if err != nil {
		memory = 0
	}
	user, err := strconv.ParseFloat(stats[13], 64)
	if err != nil {
		user = 0
	}
	system, err := strconv.ParseFloat(stats[14], 64)
	if err != nil {
		system = 0
	}
	return memory, user, system
}

func getGoMem() (uint64, uint64) {
	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)
	return mem.Alloc, mem.TotalAlloc
}

func getCgroupMemStat(parameter string) uint64 {
	data, err := ioutil.ReadFile("/sys/fs/cgroup/memory/" + parameter)
	if err != nil {
		return 0
	}
	val, err := strconv.ParseUint(strings.TrimRight(string(data), "\n"), 10, 64)
	if err != nil {
		return 0
	}
	return val
}

func getCgroupMemoryLimit() uint64 {
	return getCgroupMemStat("memory.limit_in_bytes")
}

func getCgroupMemoryUsage() uint64 {
	return getCgroupMemStat("memory.usage_in_bytes")
}

func getCgroupMemoryMax() uint64 {
	return getCgroupMemStat("memory.max_usage_in_bytes")
}

func getCgroupMemorySwapLimit() uint64 {
	return getCgroupMemStat("memory.memsw.limit_in_bytes")
}

func getCgroupMemorySwapUsage() uint64 {
	return getCgroupMemStat("memory.memsw.usage_in_bytes")
}

func getCgroupMemorySwapMax() uint64 {
	return getCgroupMemStat("memory.memsw.max_usage_in_bytes")
}

func DirSize(path string) (uint64, error) {
	var size uint64
	err := filepath.Walk(path, func(_ string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			size += uint64(info.Size())
		}
		return err
	})
	return size, err
}

func GetKernelVersion() string {
	version, err := ioutil.ReadFile("/proc/version")
	if err != nil {
		return ""
	}
	return strings.TrimRight(string(version), "\n")
}
