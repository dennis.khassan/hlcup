package entity

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"sync"
)

type ExtraType struct {
	Likes   []*AvgTimestampType
	LikedBy []uint32
}

var extraLocks sync.Map

func GetExtraLock(id uint32) *sync.RWMutex {
	val, _ := extraLocks.LoadOrStore(id, &sync.RWMutex{})
	return val.(*sync.RWMutex)
}

func StartReadExtra(id uint32) {
	GetExtraLock(id).RLock()
}

func FinishReadExtra(id uint32) {
	GetExtraLock(id).RUnlock()
}

func StartWriteExtra(id uint32) {
	GetExtraLock(id).Lock()
}

func FinishWriteExtra(id uint32) {
	GetExtraLock(id).Unlock()
}

func GetUintBytes(key uint32) []byte {
	keyBytes := make([]byte, 4)
	binary.LittleEndian.PutUint32(keyBytes, key)
	return keyBytes
}

// TODO: create ExtraType pool

func GetExtra(accountId []byte) ExtraType {
	val, err := globals.ExtraDb.Get(globals.LevelDbReadOptions, accountId)
	if err != nil {
		panic(err)
	}
	if len(val) == 0 {
		return ExtraType{}
	}

	var item ExtraType
	decoder := gob.NewDecoder(bytes.NewBuffer(val))
	err = decoder.Decode(&item)
	if err != nil {
		panic(err)
	}
	return item
}

func PutExtra(accountId []byte, extra *ExtraType) {
	var buffer bytes.Buffer
	encoder := gob.NewEncoder(&buffer)
	err := encoder.Encode(extra)
	if err != nil {
		panic(err)
	}
	err = globals.ExtraDb.Put(globals.LevelDbWriteOptions, accountId, buffer.Bytes())
	if err != nil {
		panic(err)
	}
}
