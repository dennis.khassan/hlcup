package entity

import "sort"

type AvgTimestampType struct {
	Id        int32
	Count     uint16
	Timestamp int32
}

func appendOneLike(likes *[]int32, like *LikeRaw) {
	likesLen := len(*likes)
	key := sort.Search(likesLen, func(i int) bool {
		return (*like).Id >= (*likes)[i]
	})

	if key == likesLen {
		// insert to end
		*likes = append(*likes, like.Id)
	} else {
		if (*likes)[key] != like.Id {
			// insert in middle
			*likes = append(*likes, 0)
			copy((*likes)[key+1:], (*likes)[key:])
			(*likes)[key] = like.Id
		}
	}
}

func AppendLikes(existingLikes *[]int32, newLikes []LikeRaw) {
	for _, like := range newLikes {
		appendOneLike(existingLikes, &like)
	}
}

// changes existingLikes
/*func appendOneLike(likes *[]*AvgTimestampType, like *LikeRaw) {
	likesLen := len(*likes)
	key := sort.Search(likesLen, func(i int) bool {
		return (*like).Id >= (*likes)[i].Id
	})

	if key == likesLen {
		// insert to end
		*likes = append(*likes, &AvgTimestampType{
			Id:        like.Id,
			Count:     1,
			Timestamp: like.Timestamp,
		})
	} else {
		keyLike := (*likes)[key]
		if keyLike.Id != like.Id {
			// insert in middle
			*likes = append(*likes, nil)
			copy((*likes)[key+1:], (*likes)[key:])
			(*likes)[key] = &AvgTimestampType{
				Id:        like.Id,
				Count:     1,
				Timestamp: like.Timestamp,
			}
		} else {
			// recalculate existing timestamp
			keyLike.Timestamp = int32((int64(keyLike.Timestamp)*int64(keyLike.Count) + int64(like.Timestamp)) / int64(keyLike.Count+1))
			keyLike.Count++
		}
	}
}

func AppendLikesToExtra(extra *ExtraType, likes []LikeRaw) {
	for _, like := range likes {
		appendOneLike(&extra.Likes, &like)
	}
}*/

// changes account
//func AppendLikesToAccount(account *Account, likes []LikeRaw) {
//	for _, like := range likes {
//		appendOneLike(&account.Likes, &like)
//	}
//}

/*
// use only outside existing locks (or you can catch deadlock)!
func appendOneLikedBy(accountId uint32, likes uint32) {

	StartWriteExtra(likes)

	accountKey := GetUintBytes(likes)

	extra := GetExtra(accountKey)

	key := sort.Search(len(extra.LikedBy), func(i int) bool {
		return accountId >= extra.LikedBy[i]
	})

	if key == len(extra.LikedBy) {
		extra.LikedBy = append(extra.LikedBy, accountId)
		PutExtra(accountKey, &extra)
	} else if extra.LikedBy[key] != accountId {
		extra.LikedBy = append(extra.LikedBy, 0)
		copy(extra.LikedBy[key+1:], extra.LikedBy[key:])
		extra.LikedBy[key] = accountId
		PutExtra(accountKey, &extra)
	}

	FinishWriteExtra(likes)

}

// use only outside existing locks (or you can catch deadlock)!
func AppendLikedBy(accountId uint32, extra *ExtraType) {

	for _, like := range extra.Likes {
		appendOneLikedBy(accountId, like.Id)
	}

}
*/
