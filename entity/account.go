package entity

import (
	"errors"
	"github.com/restream/reindexer"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"strconv"
)

type Premium struct {
	Start  int32 `json:"start,omitempty"`
	Finish int32 `json:"finish,omitempty"`
}

type AccountList struct {
	Accounts []*Account `json:"accounts"`
}

// atomic!
var AccountsCount = int32(0)

type Account struct {
	Id             uint32   `reindex:"id,tree,pk"json:"id"`
	Email          string   `reindex:"email,tree"json:"email"`
	EmailDomain    string   `reindex:"email_domain,hash"`
	FirstName      string   `reindex:"fname,hash"json:"fname,omitempty"`
	SecondName     string   `reindex:"sname,tree"json:"sname,omitempty"`
	SecondNameBool bool     `reindex:"sname_bool,-"`
	Phone          string   `json:"phone,omitempty"`
	PhoneCode      int32    `reindex:"phone_code,hash"`
	Sex            string   `reindex:"sex,-"json:"sex"`
	Birth          int32    `reindex:"birth,tree"json:"birth"`
	BirthYear      int32    `reindex:"birth_year,hash"`
	Country        string   `reindex:"country,hash"json:"country,omitempty"`
	City           string   `reindex:"city,hash"json:"city,omitempty"`
	Joined         uint32   `json:"joined_at"`
	JoinedYear     int32    `reindex:"joined_year,-"`
	Status         string   `reindex:"status,hash"json:"status"`
	Premium        *Premium `json:"premium,omitempty"`
	PremiumNow     bool     `reindex:"premium_now,-"`
	PremiumExisted bool     `reindex:"premium_existed,-"`
	Interests      []string `reindex:"interests,hash"`
	//LikeIds        []int32  `reindex:"like_ids,tree"`
}

// returns unsafe Account!
func GetAccountById(id int32) (*Account, error) {
	// search account
	iterator := globals.ReindexerDb.Query(globals.AccountsNamespace).
		WhereInt32("id", reindexer.EQ, int32(id)).
		Limit(1).
		MustExec().
		AllowUnsafe(true)

	defer iterator.Close()

	// get account
	if !iterator.Next() {
		return nil, errors.New("account \"" + strconv.Itoa(int(id)) + "\" not found")
	}

	return iterator.Object().(*Account), nil
}

func (acc *Account) GetAccountRaw() *AccountRaw {

	premium := PremiumRaw{}
	if acc.Premium != nil {
		premium.Start = acc.Premium.Start
		premium.Finish = acc.Premium.Finish
	}

	return &AccountRaw{
		Id:         acc.Id,
		Email:      acc.Email,
		FirstName:  acc.FirstName,
		SecondName: acc.SecondName,
		Phone:      acc.Phone,
		Sex:        acc.Sex,
		Birth:      acc.Birth,
		Country:    acc.Country,
		City:       acc.City,
		Joined:     acc.Joined,
		Status:     acc.Status,
		Interests:  acc.Interests,
		Premium:    premium,
	}

}

func GetOppositeSex(sex string) string {
	if sex == "m" {
		return "f"
	} else {
		return "m"
	}
}
