package entity

// easyjson -all entity/recommend.go

type RecommendsAccount struct {
	Id             uint32   `json:"id"`
	Email          string   `json:"email"`
	FirstName      string   `json:"fname,omitempty"`
	SecondName     string   `json:"sname,omitempty"`
	Birth          int32    `json:"birth"`
	Status         string   `json:"status"`
	Premium        *Premium `json:"premium,omitempty"`
	InterestsCount int32    `json:"-"`
}
