package entity

import (
	"gitlab.com/dennis.khassan/hlcup/globals"
	"strconv"
	"strings"
	"sync/atomic"
	"time"
)

// easyjson -all entity/raw.go

type AccountsRaw struct {
	Accounts []AccountRaw `json:"accounts"`
}

type AccountRaw struct {
	Id         uint32     `json:"id"`
	Email      string     `json:"email"`
	FirstName  string     `json:"fname"`
	SecondName string     `json:"sname"`
	Phone      string     `json:"phone"`
	Sex        string     `json:"sex"`
	Birth      int32      `json:"birth"`
	Country    string     `json:"country"`
	City       string     `json:"city"`
	Joined     uint32     `json:"joined"`
	Status     string     `json:"status"`
	Interests  []string   `json:"interests"`
	Premium    PremiumRaw `json:"premium"`
	Likes      []LikeRaw  `json:"likes"`
}

type PremiumRaw struct {
	Start  int32 `json:"start"`
	Finish int32 `json:"finish"`
}

type LikeRaw struct {
	Id        int32 `json:"id"`
	Timestamp int32 `json:"ts"`
}

type NewLikesRaw struct {
	Likes []*NewLikeRaw `json:"likes"`
}

type NewLikeRaw struct {
	Likee     int32 `json:"likee"`
	Liker     int32 `json:"liker"`
	Timestamp int32 `json:"ts"`
}

var AccountIdGenerate = false
var accountIdGenerator = uint32(0)

func (raw *AccountRaw) Validate() bool {
	if raw.Status != "свободны" &&
		raw.Status != "заняты" &&
		raw.Status != "всё сложно" {

		return false
	}
	if raw.Sex != "f" && raw.Sex != "m" {
		return false
	}

	if raw.Email != "" && !globals.EmailPattern.MatchString(raw.Email) {
		return false
	}

	return true
}

func (raw *AccountRaw) FetchDataEntities() *Account {

	if AccountIdGenerate {
		raw.Id = atomic.AddUint32(&accountIdGenerator, 1)
	}

	/*keyBytes := GetUintBytes(raw.Id)
	var extra ExtraType
	//StartWriteExtra(raw.Id)
	if !isEmptyExtra {
		extra = GetExtra(keyBytes)
	}*/

	premiumExisted := false
	isPremium := false
	premium := &Premium{}
	if raw.Premium.Start != 0 && raw.Premium.Finish != 0 {
		premium.Start = raw.Premium.Start
		premium.Finish = raw.Premium.Finish
		if globals.GlobalTimestamp > raw.Premium.Start &&
			globals.GlobalTimestamp < raw.Premium.Finish {

			isPremium = true
		}
		premiumExisted = true
	} else {
		premium = nil
	}

	//AppendLikes(&extra, raw.Likes)

	tm := time.Unix(int64(raw.Birth), 0)
	jm := time.Unix(int64(raw.Joined), 0)

	secondNameBool := false
	if raw.SecondName != "" {
		secondNameBool = true
	}

	phoneCode := 0

	foundList := globals.ValidID.FindStringSubmatch(raw.Phone)
	if len(foundList) == 2 {
		code, err := strconv.Atoi(foundList[1])
		if err == nil {
			phoneCode = code
		}
	}

	emailDomain := ""
	listEmail := strings.Split(raw.Email, "@")
	if len(listEmail) == 2 {
		emailDomain = listEmail[1]
	}

	//likes := make([]int32, 0, len(raw.Likes))
	//AppendLikes(&likes, raw.Likes)

	//PutExtra(keyBytes, &extra)
	//FinishWriteExtra(raw.Id)

	//AppendLikedBy(raw.Id, &extra)
	/*for _, like := range raw.Likes {
		globals.LikedByInitMapMutex.Lock()
		if val, ok := globals.LikedByInitMap[like.Id]; ok {
			globals.LikedByInitMap[like.Id] = append(val, raw.Id)
		} else {
			globals.LikedByInitMap[like.Id] = []uint32{raw.Id}
		}
		globals.LikedByInitMapMutex.Unlock()
	}*/

	return &Account{
		Id:             raw.Id,
		Email:          raw.Email,
		EmailDomain:    emailDomain,
		FirstName:      raw.FirstName,
		SecondName:     raw.SecondName,
		SecondNameBool: secondNameBool,
		Phone:          raw.Phone,
		PhoneCode:      int32(phoneCode),
		Sex:            raw.Sex,
		Birth:          raw.Birth,
		BirthYear:      int32(tm.Year()),
		Country:        raw.Country,
		City:           raw.City,
		Joined:         raw.Joined,
		JoinedYear:     int32(jm.Year()),
		Status:         raw.Status,
		Premium:        premium,
		PremiumNow:     isPremium,
		PremiumExisted: premiumExisted,
		Interests:      raw.Interests,
		//LikeIds:        likes,
	}
}
