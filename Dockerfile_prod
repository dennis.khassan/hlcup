FROM golang:1.11.4-alpine3.8 as builder

ARG BUILD_OPTIONS="-Ofast -march=nehalem -mmmx -mno-3dnow -msse -msse2 -msse3 -mssse3 -mno-sse4a -mcx16 -msahf -mno-movbe -mno-aes -mno-sha -mno-pclmul -mpopcnt -mno-abm -mno-lwp -mno-fma -mno-fma4 -mno-xop -mno-bmi -mno-bmi2 -mno-tbm -mno-avx -mno-avx2 -msse4.2 -msse4.1 -mno-lzcnt -mno-rtm -mno-hle -mno-rdrnd -mno-f16c -mno-fsgsbase -mno-rdseed -mno-prfchw -mno-adx -mfxsr -mno-xsave -mno-xsaveopt -mno-avx512f -mno-avx512er -mno-avx512cd -mno-avx512pf -mno-prefetchwt1 -mno-clflushopt -mno-xsavec -mno-xsaves -mno-avx512dq -mno-avx512bw -mno-avx512vl -mno-avx512ifma -mno-avx512vbmi -mno-clwb -mno-mwaitx -mno-clzero -mno-pku --param l1-cache-size=32 --param l1-cache-line-size=64 --param l2-cache-size=4096 -mtune=nehalem -pipe -DNDEBUG"
ARG CGO_OPTIONS="-Ofast -march=nehalem -msse -msse2 -msse3 -mno-avx -mno-avx2 -msse4.2 -msse4.1 -mno-avx512f -mno-avx512er -mno-avx512cd -mno-avx512pf -mno-avx512dq -mno-avx512bw -mno-avx512vl -mno-avx512ifma -mno-avx512vbmi -mtune=nehalem -pipe"

RUN apk add --no-cache git cmake make g++ musl-dev

# build leveldb
RUN cd / \
    && git clone https://github.com/google/leveldb.git \
    && cd leveldb \
    && mkdir -p build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" -DLEVELDB_BUILD_TESTS=OFF -DLEVELDB_BUILD_BENCHMARKS=OFF .. \
    && cmake --build . -- -j$(nproc) \
    && make install \
    && cd ../.. \
    && rm -rf leveldb

# build snappy
#RUN git clone https://github.com/google/snappy.git \
#    && cd snappy \
#    && mkdir -p build \
#    && cd build \
#    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" -DSNAPPY_BUILD_TESTS=OFF .. \
#    && make -j$(nproc) \
#    && make install \
#    && cd ../.. \
#    && rm -rf snappy

# build replxx
RUN git clone https://github.com/Restream/replxx.git \
    && cd replxx \
    && mkdir -p build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" .. \
    && make -j$(nproc) \
    && make install \
    && cd ../.. \
    && rm -rf replxx

RUN apk add --no-cache libexecinfo-dev

# build reindexer
RUN go get -d github.com/restream/reindexer
WORKDIR $GOPATH/src/github.com/restream/reindexer
COPY reindexer.patch ./
RUN patch -p0 < reindexer.patch

# add -lexecinfo for alpine only
RUN sed -i "s!-lreindexer !-lreindexer -lexecinfo !g" bindings/builtinserver/builtinserver_posix.go
# add -lpthread for fedora only
#RUN sed -i "s!-lreindexer !-lreindexer -lpthread !g" bindings/builtinserver/builtinserver_posix.go
# release optimizations
#RUN sed -i "s/-O2/$CGO_OPTIONS/g" bindings/builtin/builtin_posix.go
#RUN sed -i "s/-O2/$CGO_OPTIONS/g" bindings/builtinserver/builtinserver_posix.go
#RUN sed -i "s! cmake ! cmake -DCMAKE_BUILD_TYPE=Release !g" bindings/builtin/builtin_posix.go
#RUN sed -i "s! cmake ! cmake -DCMAKE_BUILD_TYPE=Release !g" bindings/builtinserver/builtinserver_posix.go

#RUN ./dependencies.sh
#RUN go generate github.com/restream/reindexer/bindings/builtin
#RUN go generate github.com/restream/reindexer/bindings/builtinserver
RUN mkdir -p build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" .. \
    && make reindexer -j$(nproc)
RUN mkdir -p build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" -DLINK_RESOURCES=On -DWITH_GPERF=Off .. \
    && make reindexer_server_library -j$(nproc)

ENV CGO_CFLAGS="$CGO_OPTIONS"

# build levigo
RUN go get -d github.com/jmhodges/levigo
WORKDIR $GOPATH/src/github.com/jmhodges/levigo
RUN touch dummy.cc
RUN go build

WORKDIR $GOPATH/src/gitlab.com/dennis.khassan/hlcup

RUN go get github.com/golang/dep/cmd/dep

COPY / $GOPATH/src/gitlab.com/dennis.khassan/hlcup

RUN dep ensure -vendor-only

#RUN cd vendor/github.com/valyala/fasthttp \
#    && patch -p0 < ../../../../fasthttp.patch

RUN go build -o /go/bin/hlcup

FROM alpine:3.8

RUN apk add --no-cache libgcc libstdc++ libexecinfo tzdata

COPY --from=builder /go/bin/hlcup /usr/local/bin/

RUN mkdir -p /tmp/leveldb/testdb

#ENV LOG_LEVEL=debug
ENV SHOW_RUNNING_TIME=30

ENTRYPOINT ["hlcup", "-disable-db-server", "-hide-http-errors", "-compact-json", "-disable-debug-contollers"]
