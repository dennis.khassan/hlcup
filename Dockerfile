FROM golang:1.11.4-alpine3.8 as builder

ARG BUILD_OPTIONS="-Ofast -march=native -pipe -DNDEBUG"

RUN apk add --no-cache git cmake make g++ musl-dev

# build leveldb
RUN cd / \
    && git clone https://github.com/google/leveldb.git \
    && cd leveldb \
    && mkdir -p build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" -DLEVELDB_BUILD_TESTS=OFF -DLEVELDB_BUILD_BENCHMARKS=OFF .. \
    && cmake --build . -- -j$(nproc) \
    && make install \
    && cd ../.. \
    && rm -rf leveldb

# build snappy
#RUN git clone https://github.com/google/snappy.git \
#    && cd snappy \
#    && mkdir -p build \
#    && cd build \
#    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" -DSNAPPY_BUILD_TESTS=OFF .. \
#    && make -j$(nproc) \
#    && make install \
#    && cd ../.. \
#    && rm -rf snappy

# build replxx
RUN git clone https://github.com/Restream/replxx.git \
    && cd replxx \
    && mkdir -p build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" .. \
    && make -j$(nproc) \
    && make install \
    && cd ../.. \
    && rm -rf replxx

RUN apk add --no-cache libexecinfo-dev

# build reindexer
RUN go get -d github.com/restream/reindexer
WORKDIR $GOPATH/src/github.com/restream/reindexer
COPY reindexer.patch ./
RUN patch -p0 < reindexer.patch

# add -lexecinfo for alpine only
RUN sed -i "s!-lreindexer !-lreindexer -lexecinfo !g" bindings/builtinserver/builtinserver_posix.go
# add -lpthread for fedora only
#RUN sed -i "s!-lreindexer !-lreindexer -lpthread !g" bindings/builtinserver/builtinserver_posix.go
# release optimizations
#RUN sed -i "s/-O2/$BUILD_OPTIONS/g" bindings/builtin/builtin_posix.go
#RUN sed -i "s/-O2/$BUILD_OPTIONS/g" bindings/builtinserver/builtinserver_posix.go
#RUN sed -i "s! cmake ! cmake -DCMAKE_BUILD_TYPE=Release !g" bindings/builtin/builtin_posix.go
#RUN sed -i "s! cmake ! cmake -DCMAKE_BUILD_TYPE=Release !g" bindings/builtinserver/builtinserver_posix.go

#RUN ./dependencies.sh
#RUN go generate github.com/restream/reindexer/bindings/builtin
#RUN go generate github.com/restream/reindexer/bindings/builtinserver
RUN mkdir -p build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" .. \
    && make reindexer -j$(nproc)
RUN mkdir -p build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Opt -DCMAKE_CXX_FLAGS_OPT:STRING="$BUILD_OPTIONS" -DLINK_RESOURCES=On -DWITH_GPERF=Off .. \
    && make reindexer_server_library -j$(nproc)

ENV CGO_CFLAGS="$BUILD_OPTIONS"

# build levigo
RUN go get -d github.com/jmhodges/levigo
WORKDIR $GOPATH/src/github.com/jmhodges/levigo
RUN touch dummy.cc
RUN go build

WORKDIR $GOPATH/src/gitlab.com/dennis.khassan/hlcup

RUN go get github.com/golang/dep/cmd/dep
RUN go get github.com/mailru/easyjson/...

RUN apk add --no-cache curl

RUN mkdir -p /tmp/leveldb/testdb
