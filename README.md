# hlcup

Highload Cup 2

Для локальной компиляции необходимо пройти процедуру установки модулей для ReindexDb, описанной на

https://github.com/Restream/reindexer#installation-for-embeded-mode

`make build` — собрать контейнер для локальной разработки (с собранным reindexer)

`make` — запустить контейнер для локальной разработки

`make build_prod` — собрать боевой контейнер

`make run_prod` — запустить боевой контейнер

`make push_prod` — отправить боевой контейнер (TODO)

**Локальная разработка:**

одноразово: `make build`, `dep ensure --vendor-only`

для открытия окружения разработки: `make`

для сборки (команды внутри контейнера): `go build`, `hlcup`