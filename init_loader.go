package main

import (
	"archive/zip"
	"bufio"
	"errors"
	"github.com/spf13/cast"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"io/ioutil"
	"os"
	"strings"
)

const (
	FilesPath      = "/tmp/data/data.zip"
	DataFilePrefix = "accounts_"
)

func getAccountsGenerator(accountParsedChan chan *entity.AccountsRaw) error {
	file, err := os.Stat(FilesPath)
	if err != nil {
		return err
	}
	globals.Logger.Sugar().Info("File unzip start (", file.Size()/1024/1024, "MB)")

	zipReader, err := zip.OpenReader(FilesPath)
	if err != nil {
		return err
	}
	filesCount := 0
	for _, file := range zipReader.File {
		var accountsParse *entity.AccountsRaw
		{
			fileDescriptor, err := file.Open()
			if err != nil {
				return err
			}
			if file.FileInfo().IsDir() {
				return errors.New("directory inside zip")
			}

			if !strings.HasPrefix(file.FileInfo().Name(), DataFilePrefix) {
				continue
			}

			accountsJsonBytes, err := ioutil.ReadAll(fileDescriptor)
			if err != nil {
				return err
			}

			err = fileDescriptor.Close()
			if err != nil {
				return err
			}

			accountsParse = &entity.AccountsRaw{}
			globals.Logger.Sugar().Debug(len(accountsJsonBytes))
			err = accountsParse.UnmarshalJSON(accountsJsonBytes)
			if err != nil {
				return err
			}
		}

		globals.Logger.Sugar().Debugf("File %s loading", file.FileInfo().Name())
		accountParsedChan <- accountsParse
		filesCount++
	}

	globals.Logger.Sugar().Info("Found ", filesCount, " files")

	close(accountParsedChan)

	return zipReader.Close()
}

func loadCurrentTimestamp() error {
	file, err := os.Open(OptionsFilePath)
	if err != nil {
		globals.Logger.Sugar().Error(err)

		return err
	}

	fileReader := bufio.NewReader(file)
	timestampBytes, _, err := fileReader.ReadLine()
	timestampString := cast.ToString(timestampBytes)

	globals.GlobalTimestamp = cast.ToInt32(timestampString)

	err = file.Close()
	if err != nil {
		globals.Logger.Sugar().Error(err)

		return err
	}

	return nil
}
