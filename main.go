package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/buaazp/fasthttprouter"
	"github.com/restream/reindexer"
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/controller"
	"gitlab.com/dennis.khassan/hlcup/db"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	_ "net/http/pprof"
	"os"
	"runtime"
	"runtime/pprof"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

const (
	OptionsFilePath = "/tmp/data/options.txt"
	loadersCount    = 3

	RequestTimeout = 2 * time.Second
)

var (
	disableDbServerPtr         = flag.Bool("disable-db-server", false, "disables database server")
	hideHttpErrorsPtr          = flag.Bool("hide-http-errors", false, "clears body for 4xx pages")
	compactJsonPtr             = flag.Bool("compact-json", false, "compacts responses JSON")
	disableDiskStatsPtr        = flag.Bool("disable-disk-stats", false, "disable disk usage reports")
	outputToConsolePtr         = flag.Bool("output-to-console", false, "outputs responses to console")
	disableDebugControllersPtr = flag.Bool("disable-debug-contollers", false, "disable debug controllers")
	enableIdGeneratorPtr       = flag.Bool("generate-id", false, "enable accounts id generator")
	cpuprofile                 = flag.String("cpuprofile", "", "write cpu profile to `file`")
	memprofile                 = flag.String("memprofile", "", "write memory profile to `file`")

	timeLoc *time.Location
)

func init() {

	var err error
	timeLoc, err = time.LoadLocation(globals.TimeZone)
	if err != nil {
		panic(err)
	}

	logLevel := os.Getenv("LOG_LEVEL")
	if logLevel == "" {
		logLevel = "info"
	}
	rawJSON := []byte(`{
	  "level": "` + logLevel + `",
	  "encoding": "console",
	  "outputPaths": ["stdout"],
	  "errorOutputPaths": ["stderr"],
	  "encoderConfig": {
	    "messageKey": "message",
	    "timeKey": "time",
	    "levelKey": "level",
	    "levelEncoder": "lowercase"
	  }
	}`)

	var cfg zap.Config
	if err := json.Unmarshal(rawJSON, &cfg); err != nil {
		panic(err)
	}
	cfg.EncoderConfig.EncodeTime = timeEncoder
	globals.Logger, err = cfg.Build()
	if err != nil {
		panic(err)
	}

	flag.Parse()

	if *hideHttpErrorsPtr {
		controller.HideHttpErrors = true
	}
	if *outputToConsolePtr {
		controller.OutputToConsole = true
	}
	if *compactJsonPtr {
		controller.CompactJson = true
	}
	if *enableIdGeneratorPtr {
		entity.AccountIdGenerate = true
	}

}

func main() {
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			panic(err)
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			panic(err)
		}
	}

	// show starting message
	timeStart := time.Now()
	globals.Logger.Sugar().Info(
		"Starting application with ", getProcCount(), " (", runtime.GOMAXPROCS(0), ") threads, ",
		fmt.Sprintf("%.1f", float32(getMemoryMaxPages()*getMemoryPageSize())/1024/1024/1024), "GB total memory (",
		getCgroupMemoryLimit()/1024/1024, "/", getCgroupMemorySwapLimit()/1024/1024, "MB)",
	)
	globals.Logger.Sugar().Info(GetKernelVersion())

	// show status messages
	runningTimeStr := os.Getenv("SHOW_RUNNING_TIME")
	if runningTimeStr != "" {
		runningTime, err := strconv.Atoi(runningTimeStr)
		if err == nil {
			go printStats(timeStart, runningTime)
		} else {
			globals.Logger.Sugar().Error("error converting SHOW_RUNNING_TIME=\"" + runningTimeStr + "\" to number")
		}
	}

	// load options
	go func() {
		err := loadCurrentTimestamp()
		if err != nil {
			globals.Logger.Sugar().Fatal("Can't load timestamp from options.txt: " + err.Error())
		}
	}()

	// initialize DB
	dbExists := false
	if _, err := os.Stat(db.Path); !os.IsNotExist(err) {
		dbExists = true
	}
	err := db.Init(*disableDbServerPtr)
	if err != nil {
		globals.Logger.Sugar().Fatal(err)
	}

	// fill DB if it is empty
	if !dbExists {
		accountsChan := make(chan *entity.AccountsRaw)
		go func() {
			err := getAccountsGenerator(accountsChan)
			if err != nil {
				globals.Logger.Sugar().Error(err)
			}
		}()

		var wg sync.WaitGroup
		wg.Add(loadersCount)

		// parse accounts
		for i := 0; i < loadersCount; i++ {
			go func() {
				for {
					accountsRaw, ok := <-accountsChan
					if !ok {
						break
					}
					db.LoadAccountsToDatabase(accountsRaw)
				}
				wg.Done()
			}()
		}

		wg.Wait()
		timeDb := time.Now()
		globals.Logger.Sugar().Info("Database loaded in " + globals.GetDurationStr(timeDb.Sub(timeStart)))

		/*
			// store likedBy
			for like, ids := range globals.LikedByInitMap {
				sort.Slice(ids, func(i, j int) bool { return ids[j] < ids[i] })
				likeBytes := entity.GetUintBytes(like)
				extra := entity.GetExtra(likeBytes)
				extra.LikedBy = ids
				entity.PutExtra(likeBytes, &extra)
			}
			globals.LikedByInitMap = map[uint32][]uint32{}

			// clear some memory
			runtime.GC()
		*/

		// create extra lock map
		//for i := int32(1); i <= entity.AccountsCount; i++ {
		//	entity.GetExtraLock(uint32(i))
		//}

		//timeStore := time.Now()
		//globals.Logger.Sugar().Info("Additional data stored in " + globals.GetDurationStr(timeStore.Sub(timeDb)))

		// create indexex in reindexer
		//createIndexes()

		//globals.Logger.Sugar().Info("Indexes created in " + globals.GetDurationStr(time.Now().Sub(timeStore)))
	} else {
		// load accounts count for stat
		iterator := globals.ReindexerDb.Query(globals.AccountsNamespace).Exec()
		atomic.StoreInt32(&entity.AccountsCount, int32(iterator.Count()))
		iterator.Close()

		globals.Logger.Sugar().Info("Loaded existing database in " + globals.GetDurationStr(time.Now().Sub(timeStart)))
	}

	*disableDiskStatsPtr = true

	router := initRouter()

	if *cpuprofile != "" {
		pprof.StopCPUProfile()
	}

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			panic(err)
		}
		runtime.GC() // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			panic(err)
		}
		err = f.Close()
		if err != nil {
			panic(err)
		}
	}

	globals.Logger.Sugar().Info("Starting HTTP server")

	err = fasthttp.ListenAndServe(":80", router.Handler)
	globals.Logger.Sugar().Fatal(err)

	_ = globals.Logger.Sync()
}

func initRouter() *fasthttprouter.Router {
	router := fasthttprouter.New()
	router.RedirectTrailingSlash = false
	//router.GET("/accounts/filter/", controller.FilterAction)
	//router.GET("/accounts/group/", controller.GroupAction)
	//router.POST("/accounts/new/", controller.NewAction)
	//router.POST("/accounts/likes/", controller.LikesAction)
	router.GET("/accounts/:id/", routeGET)
	router.POST("/accounts/:id/", routePOST)
	router.GET("/accounts/:id/recommend/", controller.RecommendAction)
	router.GET("/accounts/:id/suggest/", controller.SuggestAction)
	if !*disableDebugControllersPtr {
		router.GET("/extra/:id/", controller.ExtraAction)
	}

	if *hideHttpErrorsPtr {
		router.MethodNotAllowed = controller.Response400
		router.NotFound = controller.Response404
	}

	return router
}

func routeGET(ctx *fasthttp.RequestCtx) {
	route := ctx.UserValue("id")
	switch route {
	case "filter":
		controller.FilterAction(ctx)
	case "group":
		controller.GroupAction(ctx)
	default:
		if *hideHttpErrorsPtr {
			controller.Response404(ctx)
		} else {
			ctx.SetStatusCode(fasthttp.StatusNotFound)
			ctx.SetBodyString(fasthttp.StatusMessage(fasthttp.StatusNotFound))
		}
	}
}

func routePOST(ctx *fasthttp.RequestCtx) {
	route := ctx.UserValue("id")
	switch route {
	case "new":
		controller.NewAction(ctx)
	case "likes":
		controller.LikesAction(ctx)
	default:
		controller.UpdateAction(ctx)
	}
}

func timeoutMiddleware(next fasthttp.RequestHandler) fasthttp.RequestHandler {

	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {

		doneCh := make(chan struct{})
		go func() {
			next(ctx)
			time.Sleep(1 * time.Second)
			close(doneCh)
		}()

		select {
		case <-doneCh:
		case <-time.After(RequestTimeout):
			ctx.TimeoutError("")
		}
	})
}

func timeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.In(timeLoc).Format(globals.TimeFormat))
}

func printStats(timeStart time.Time, delay int) {
	pageSize := getMemoryPageSize()
	ticks := float64(getClockTicks())
	procCount := float64(getProcCount())
	prevUser := .0
	prevSystem := .0
	prevTime := time.Now()
	for {
		time.Sleep(time.Duration(delay) * time.Second)
		curTime := time.Now()
		goAlloc, goTotal := getGoMem()
		memory, user, system := getUsage()
		var dbSize uint64
		if !*disableDiskStatsPtr {
			var err error
			dbSize, err = DirSize(db.Path)
			if err != nil {
				globals.Logger.Sugar().Error("Error getting database size: ", err)
			}
		} else {
			dbSize = 0
		}
		globals.Logger.Sugar().Info("uptime: ", globals.GetDurationStr(curTime.Sub(timeStart)),
			", accounts: ", atomic.LoadInt32(&entity.AccountsCount)/1000,
			"k, mem: ", memory*uint64(pageSize)/1024/1024, "MB",
			", db: ", dbSize/1024/1024, "MB",
			", go: ", goAlloc/1024/1024, "/", goTotal/1024/1024, "MB",
			", cpu: ", int((user+system-prevUser-prevSystem)/ticks*100/float64(curTime.Sub(prevTime)/time.Second)/procCount),
			"% (", globals.GetDurationStr(time.Duration(user/ticks)*time.Second),
			"/", globals.GetDurationStr(time.Duration(system/ticks)*time.Second), ")",
			//", ", atomic.LoadUint64(&globals.CntFilter), "/", atomic.LoadUint64(&globals.CntGroup), "/", atomic.LoadUint64(&globals.CntLikes),
			//"/", atomic.LoadUint64(&globals.CntNew), "/", atomic.LoadUint64(&globals.CntRecommend), "/", atomic.LoadUint64(&globals.CntSuggest),
			//"/", atomic.LoadUint64(&globals.CntUpdate),
		)
		prevUser = user
		prevSystem = system
		prevTime = curTime
	}
}

func createIndexes() {

	// index
	err := globals.ReindexerDb.UpdateIndex(globals.AccountsNamespace, reindexer.IndexDef{
		Name:      "id",
		JSONPaths: []string{"id"},
		IndexType: "tree",
		FieldType: "int",
		IsPK:      true,
	})

	err = globals.ReindexerDb.AddIndex(globals.AccountsNamespace,
		// email
		reindexer.IndexDef{
			Name:      "email",
			JSONPaths: []string{"email"},
			IndexType: "tree",
			FieldType: "string",
		},
		// email domain
		reindexer.IndexDef{
			Name:      "email_domain",
			JSONPaths: []string{"email_domain"},
			IndexType: "-",
			FieldType: "string",
		},
		// first name
		reindexer.IndexDef{
			Name:      "fname",
			JSONPaths: []string{"fname"},
			IndexType: "hash",
			FieldType: "string",
		},
		// second name
		reindexer.IndexDef{
			Name:      "sname",
			JSONPaths: []string{"sname"},
			IndexType: "tree",
			FieldType: "string",
		},
		// second name bool
		reindexer.IndexDef{
			Name:      "sname_bool",
			JSONPaths: []string{"sname_bool"},
			IndexType: "-",
			FieldType: "bool",
		},
		// phone code
		reindexer.IndexDef{
			Name:      "phone_code",
			JSONPaths: []string{"phone_code"},
			IndexType: "-",
			FieldType: "int",
		},
		// sex
		reindexer.IndexDef{
			Name:      "sex",
			JSONPaths: []string{"sex"},
			IndexType: "-",
			FieldType: "string",
		},
		// birth
		reindexer.IndexDef{
			Name:      "birth",
			JSONPaths: []string{"birth"},
			IndexType: "tree",
			FieldType: "int",
		},
		// birth year
		reindexer.IndexDef{
			Name:      "birth_year",
			JSONPaths: []string{"birth_year"},
			IndexType: "-",
			FieldType: "int",
		},
		// country
		reindexer.IndexDef{
			Name:      "country",
			JSONPaths: []string{"country"},
			IndexType: "hash",
			FieldType: "string",
		},
		// city
		reindexer.IndexDef{
			Name:      "city",
			JSONPaths: []string{"city"},
			IndexType: "hash",
			FieldType: "string",
		},
		// joined year
		reindexer.IndexDef{
			Name:      "joined_year",
			JSONPaths: []string{"joined_year"},
			IndexType: "-",
			FieldType: "int",
		},
		// status
		reindexer.IndexDef{
			Name:      "status",
			JSONPaths: []string{"status"},
			IndexType: "-",
			FieldType: "string",
		},
		// premium is now
		reindexer.IndexDef{
			Name:      "premium_now",
			JSONPaths: []string{"premium_now"},
			IndexType: "-",
			FieldType: "bool",
		},
		// premium is existed
		reindexer.IndexDef{
			Name:      "premium_existed",
			JSONPaths: []string{"premium_existed"},
			IndexType: "-",
			FieldType: "bool",
		},
	)
	if err != nil {
		panic(err)
	}
}
