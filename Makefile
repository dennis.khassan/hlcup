APP=hlcup
TAG=latest

# Metadata about this makefile and position
MKFILE_PATH := $(lastword $(MAKEFILE_LIST))
CURRENT_DIR := $(dir $(realpath $(MKFILE_PATH)))
CURRENT_DIR := $(CURRENT_DIR:/=)

.DEFAULT_GOAL := run

MEMORY_LIMIT=500m

DOCKER_REPO=stor.highloadcup.ru/accounts/polar_falcon

build:
	docker build --compress --force-rm --pull -t "${APP}-dev:${TAG}" .

run:
	docker run --rm -it -m ${MEMORY_LIMIT} --memory-swap ${MEMORY_LIMIT} -p 9088:9088 -p 9080:80 --entrypoint=/tmp/data/entrypoint.sh -v $(shell pwd)/data:/tmp/data:ro -v $(shell pwd):/go/src/gitlab.com/dennis.khassan/hlcup --name "${APP}-dev" "${APP}-dev:${TAG}"

exec:
	docker exec -it "${APP}-dev" sh

build_prod:
	docker build -f Dockerfile_prod --compress --force-rm --pull -t "${APP}:${TAG}" .

push_prod: build_prod
	docker tag "${APP}:${TAG}" "${DOCKER_REPO}"
	docker push "${DOCKER_REPO}"

run_prod:
	docker run --rm -it -m ${MEMORY_LIMIT} --memory-swap ${MEMORY_LIMIT} -p 9080:80 -v $(shell pwd)/data:/tmp/data:ro --name "${APP}" "${APP}:${TAG}"
