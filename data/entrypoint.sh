#!/bin/sh

echo "ensuring dependencies..."
dep ensure -vendor-only
export LOG_LEVEL=debug
echo -e 'easyjson -all entity/raw.go\nrm -rf /tmp/reindex /tmp/leveldb/testdb/* && go build && ./hlcup' > ~/.ash_history
sh
