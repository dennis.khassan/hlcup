package db

import (
	"github.com/jmhodges/levigo"
	"github.com/restream/reindexer"
	_ "github.com/restream/reindexer/bindings/builtin"
	_ "github.com/restream/reindexer/bindings/builtinserver"
	"github.com/restream/reindexer/bindings/builtinserver/config"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"sync/atomic"
	"time"
)

const (
	Path        = "/tmp/reindex/testdb"
	LevelDbPath = "/tmp/leveldb/testdb"
)

func Init(disableDbServer bool) error {

	if disableDbServer {
		globals.ReindexerDb = reindexer.NewReindex("builtin://" + Path)
	} else {
		serverConfig := config.DefaultServerConfig()
		globals.ReindexerDb = reindexer.NewReindex("builtinserver://testdb", reindexer.WithServerConfig(100*time.Second, serverConfig))
	}

	var err error

	err = globals.ReindexerDb.OpenNamespace(globals.AccountsNamespace, reindexer.DefaultNamespaceOptions(), entity.Account{})
	if err != nil {
		return err
	}

	globals.LevelDbOptions = levigo.NewOptions()
	globals.LevelDbOptions.SetCreateIfMissing(true)
	globals.LevelDbOptions.SetErrorIfExists(false)
	globals.LevelDbOptions.SetParanoidChecks(false)

	globals.LevelDbReadOptions = levigo.NewReadOptions()
	globals.LevelDbReadOptions.SetVerifyChecksums(false)

	globals.LevelDbWriteOptions = levigo.NewWriteOptions()
	globals.LevelDbWriteOptions.SetSync(false)

	globals.ExtraDb, err = levigo.Open(LevelDbPath+"/"+globals.ExtraNamespace, globals.LevelDbOptions)
	if err != nil {
		return err
	}

	return nil

}

func LoadAccountsToDatabase(accountsRaw *entity.AccountsRaw) {
	for _, accountRaw := range accountsRaw.Accounts {

		account := accountRaw.FetchDataEntities()

		atomic.AddInt32(&entity.AccountsCount, 1)

		cnt, err := globals.ReindexerDb.Insert(globals.AccountsNamespace, account)
		if err != nil {
			globals.Logger.Sugar().Error(err)
		}
		if cnt != 1 {
			panic("Account not inserted")
		}

	}
}
