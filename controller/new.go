package controller

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"sync/atomic"
)

func NewAction(ctx *fasthttp.RequestCtx) {
	//atomic.AddUint64(&globals.CntNew, 1)

	requestJson := ctx.Request.Body()

	accountRaw := &entity.AccountRaw{}
	err := accountRaw.UnmarshalJSON(requestJson)
	if err != nil || !accountRaw.Validate() {
		Response400(ctx)
		return
	}
	if accountRaw.Id == 0 {
		Response400(ctx)

		return
	}

	go func(accRaw *entity.AccountRaw) {
		account := accRaw.FetchDataEntities()

		isInserted, err := globals.ReindexerDb.Insert(globals.AccountsNamespace, account)
		if err != nil {
			panic(err)
		}
		if isInserted == 0 {
			//ctx.Response.SetStatusCode(400)
			return
		}
		if !globals.DataUpdated {
			globals.DataUpdated = true
		}
		atomic.AddInt32(&entity.AccountsCount, 1)
	}(accountRaw)

	//accFinal, found := globals.ReindexerDb.Query(globals.AccountsNamespace).
	//	Where("id", reindexer.EQ, account.Id).
	//	Get()
	//
	//if !found {
	//	ctx.Response.SetStatusCode(202)
	//	ctx.Response.SetBody([]byte("Error"))
	//}
	//accFinalT := accFinal.(*entity.Account)
	//body, _ := json.Marshal(accFinalT)
	//ctx.Response.SetBody(body)

	ctx.Response.SetStatusCode(201)
	ctx.Response.SetBody(globals.EmptyJsonString)

	return
}
