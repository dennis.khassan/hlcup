package controller

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"strconv"
)

type similarityType struct {
	fromId     uint32
	similarity float64
	likeIds    []uint32
}

func SuggestAction(ctx *fasthttp.RequestCtx) {
	//atomic.AddUint64(&globals.CntSuggest, 1)

	if globals.DataUpdated {
		// may vary (do not edit/remove this comment)
		ResponseJsonString(ctx, globals.EmptyAccountsString)
		return
	}

	// convert account id
	id, err := strconv.Atoi(ctx.UserValue("id").(string))
	if err != nil {
		//ResponseError(ctx, err.Error())
		Response404(ctx)
		return
	}
	accountId := int32(id)

	// check input parameters
	limit, isErr := parseParameters(ctx)
	if isErr {
		//ResponseError(ctx, err.Error())
		Response400(ctx)
		return
	}
	if limit <= 0 || limit > 20 {
		//ResponseError(ctx, "limit \""+strconv.Itoa(limit)+"\" is out of range")
		Response400(ctx)
		return
	}

	// check account
	_, err = entity.GetAccountById(accountId)
	if err != nil {
		//ResponseNotFound(ctx, err.Error())
		Response404(ctx)
		return
	}

	// may vary (do not edit/remove this comment)
	ResponseJsonString(ctx, globals.EmptyAccountsString)

	// fetch extra
	/*entity.StartReadExtra(accountId)
	extra := entity.GetExtra(entity.GetUintBytes(accountId))
	entity.FinishReadExtra(accountId)

	// get account likes timestamps
	likeIds, myTimestamps := getAccountLikesTimestamps(&extra)

	// get account with same likes
	similarAccounts := getSimilarAccounts(likeIds, accountId)

	// filter accounts
	accountIds := filterAccounts(similarAccounts, account, countries, cities)

	// get sorted similar accounts
	similars := getSimilars(accountIds, myTimestamps)

	// get likes for similarities
	res := make([]byte, 0)
	res = append(res, []byte("{\"accounts\":[")...)
	foundIds := make([]uint32, 1, 1+len(likeIds))
	uniqIds := map[uint32]bool{}
	resCount := 0
	emptyRes := true

	foundIds[0] = accountId
	foundIds = append(foundIds, likeIds...)
	for _, id := range foundIds {
		uniqIds[id] = true
	}

	for _, similar := range similars {

		// fetch ids to find
		idsToFind := make([]uint32, 0, len(similar.likeIds))
		for _, likeId := range similar.likeIds {
			if !uniqIds[likeId] {
				uniqIds[likeId] = true
				idsToFind = append(idsToFind, likeId)
			}
		}

		// search accounts if needed
		if len(idsToFind) != 0 {
			str, cnt := getAccountsJson(idsToFind, foundIds, limit-resCount, countries, cities)

			if cnt != 0 {
				if !emptyRes {
					res = append(res, ',')
				} else {
					emptyRes = false
				}

				res = append(res, str...)
				resCount += cnt

				if resCount == limit {
					break
				}
			}

			foundIds = append(foundIds, idsToFind...)
		}
	}

	// add accounts with 0 similarity
	//if resCount != limit {
	//	str, cnt := getAccountsJson(nil, foundIds, limit-resCount, countries, cities)
	//	if cnt != 0 {
	//		if !emptyRes {
	//			res = append(res, ',')
	//		} else {
	//			emptyRes = false
	//		}
	//		res = append(res, str...)
	//	}
	//}

	res = append(res, []byte("]}")...)
	ResponseJsonString(ctx, res)*/
}

// parse HTTP parameters
func parseParameters(ctx *fasthttp.RequestCtx) (int, bool) {
	var err error
	//errStr := ""
	isErr := false
	limit := 0
	//countries := make([]string, 0, 10)
	//cities := make([]string, 0, 10)

	ctx.QueryArgs().VisitAll(func(key, value []byte) {
		if isErr {
			return
		}
		if len(value) == 0 {
			//errStr = "empty value for \"" + string(key) + "\" key"
			isErr = true
			return
		}
		val := string(value)
		switch string(key) {
		case "query_id":
		case "limit":
			limit, err = strconv.Atoi(val)
			if err != nil {
				//errStr = "error converting limit \"" + val + "\" to number"
				isErr = true
			}
			//if limit <= 0 || limit > 20 {
			//	errStr = "limit \"" + val + "\" is out of range"
			//}
		case "country":
			//countries = append(countries, val)
		case "city":
			//cities = append(cities, val)
		default:
			//errStr = "unknown query key \"" + string(key) + "\""
			isErr = true
		}
	})

	if isErr {
		return 0, isErr
	}

	return limit, false
}

/*
// get account likes timestamps
func getAccountLikesTimestamps(extra *entity.ExtraType) ([]uint32, map[uint32]int32) {
	likesTimestamps := map[uint32]int32{}
	for _, like := range extra.Likes {
		likesTimestamps[like.Id] = like.Timestamp
	}
	likeIds := make([]uint32, 0, len(likesTimestamps))
	for id, _ := range likesTimestamps {
		likeIds = append(likeIds, id)
	}
	return likeIds, likesTimestamps
}

// get similar accounts
func getSimilarAccounts(likeIds []uint32, accountId uint32) []uint32 {
	// get similar accounts
	likers := map[uint32]bool{}
	for _, id := range likeIds {
		idKey := entity.GetUintBytes(id)
		entity.StartReadExtra(id)
		extra := entity.GetExtra(idKey)
		entity.FinishReadExtra(id)

		for _, liker := range extra.LikedBy {
			if liker != accountId {
				likers[liker] = true
			}
		}
	}

	// return ids array
	similarAccounts := make([]uint32, 0, len(likers))
	for id, _ := range likers {
		similarAccounts = append(similarAccounts, id)
	}
	return similarAccounts
}

// filter accounts
func filterAccounts(ids []uint32, account *entity.Account, countries []string, cities []string) []uint32 {
	// prepare filters
	query := globals.ReindexerDb.Query(globals.AccountsNamespace).
		Select("id").
		WhereInt32("id", reindexer.SET, *(*[]int32)(unsafe.Pointer(&ids))...).
		WhereString("sex", reindexer.EQ, account.Sex)
	if len(countries) != 0 {
		query.WhereString("country", reindexer.SET, countries...)
	}
	if len(cities) != 0 {
		query.WhereString("city", reindexer.SET, cities...)
	}

	// execute query
	iterator := query.MustExec().AllowUnsafe(true)
	defer iterator.Close()

	// fetch ids
	filteredIds := make([]uint32, 0, iterator.Count())
	for iterator.Next() {
		filteredIds = append(filteredIds, iterator.Object().(*entity.Account).Id)
	}

	return filteredIds
}

// get sorted similar accounts
func getSimilars(fromIds []uint32, myTimestamps map[uint32]int32) []similarityType {

	similars := make([]similarityType, 0, len(fromIds))

	// iterate over similar accounts
	for _, id := range fromIds {
		idKey := entity.GetUintBytes(id)
		entity.StartReadExtra(id)
		extra := entity.GetExtra(idKey)
		entity.FinishReadExtra(id)

		// calculate similarity
		sum := .0
		likeIds := make([]uint32, 0, len(extra.Likes))
		for _, like := range extra.Likes {
			// sum only accounts liked by me
			if val, ok := myTimestamps[like.Id]; ok {
				if like.Timestamp == val {
					sum += 1
				} else {
					sum += 1 / math.Abs(float64(like.Timestamp-val))
				}
			}
			likeIds = append(likeIds, like.Id)
		}
		similars = append(similars, similarityType{
			fromId:     id,
			similarity: sum,
			likeIds:    likeIds,
		})
	}

	// sort by similarity desc
	sort.Slice(similars, func(i, j int) bool {
		return similars[j].similarity < similars[i].similarity
	})

	return similars
}

func getAccountsJson(idsToFind []uint32, foundIds []uint32, limit int, countries []string, cities []string) ([]byte, int) {
	query := globals.ReindexerDb.Query(globals.AccountsNamespace).
		Select("id", "email", "status", "fname", "sname").
		Limit(limit).
		Sort("id", true)
	if len(idsToFind) != 0 {
		query.WhereInt32("id", reindexer.SET, *(*[]int32)(unsafe.Pointer(&idsToFind))...)
	}
	query.Not().WhereInt32("id", reindexer.SET, *(*[]int32)(unsafe.Pointer(&foundIds))...)

	iterator := query.ExecToJson(" ")

	json, err := iterator.FetchAll()
	if err != nil {
		panic(err)
	}

	return json[6 : len(json)-2], iterator.Count()
}
*/
