package controller

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"strconv"
)

func ExtraAction(ctx *fasthttp.RequestCtx) {
	// convert account id
	id, err := strconv.Atoi(ctx.UserValue("id").(string))
	if err != nil {
		ResponseError(ctx, err.Error())
		return
	}
	accountId := uint32(id)

	// fetch extra
	entity.StartReadExtra(accountId)
	extra := entity.GetExtra(entity.GetUintBytes(accountId))
	entity.FinishReadExtra(accountId)

	res, err := json.Marshal(extra)
	if err != nil {
		panic(err)
	}

	ResponseJsonString(ctx, res)
}
