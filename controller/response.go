package controller

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/globals"
)

var HideHttpErrors = false
var OutputToConsole = false
var CompactJson = false

func Response400(ctx *fasthttp.RequestCtx) {
	ctx.SetStatusCode(400)
}

func Response404(ctx *fasthttp.RequestCtx) {
	ctx.SetStatusCode(404)
}

func ResponseError(ctx *fasthttp.RequestCtx, data string) {
	Response400(ctx)
	globals.Logger.Sugar().Debug("error in url: " + string(ctx.RequestURI()) + ": " + data)
	if !HideHttpErrors {
		ctx.SetBody([]byte(data))
	}
}

func ResponseNotFound(ctx *fasthttp.RequestCtx, data string) {
	Response404(ctx)
	globals.Logger.Sugar().Debug("not found url: " + string(ctx.RequestURI()) + ": " + data)
	if !HideHttpErrors {
		ctx.SetBody([]byte(data))
	}
}

func ResponseJsonString(ctx *fasthttp.RequestCtx, data []byte) {
	ctx.Response.Header.Set("Content-Type", "application/json")
	if CompactJson {
		ctx.SetBody(data)
	} else {
		var prettyJSON bytes.Buffer
		err := json.Indent(&prettyJSON, data, "", "  ")
		if err != nil {
			globals.Logger.Fatal("JSON parse error: " + err.Error())
		}
		ctx.SetBody(prettyJSON.Bytes())
	}
	if OutputToConsole {
		fmt.Println(data)
	}
}

func JSONWriter(ctx *fasthttp.RequestCtx, code int, obj interface{}) {
	ctx.Response.Header.SetCanonical([]byte("application/json"), []byte("Content-Type"))
	ctx.Response.SetStatusCode(code)
	if err := json.NewEncoder(ctx).Encode(obj); err != nil {
		ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}
