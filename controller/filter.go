package controller

import (
	"github.com/restream/reindexer"
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"strconv"
	"strings"
)

func FilterAction(ctx *fasthttp.RequestCtx) {
	//atomic.AddUint64(&globals.CntFilter, 1)

	//errStr := ""
	isErr := false
	isLike := false

	fields := make(map[string]bool)
	fields["id"] = true
	fields["email"] = true

	query := globals.ReindexerDb.Query(globals.AccountsNamespace)
	ctx.QueryArgs().VisitAll(func(key, value []byte) {
		if isErr {
			return
		}

		Key := string(key)
		Value := string(value)

		if Value == "" {
			//errStr = "Empty value"
			isErr = true
			return
		}

		field, err, like := whereBuilder(Key, Value, query)
		if err {
			//errStr = err
			isErr = true
			if like {
				isLike = true
			}
			return
		}

		if field != "" {
			fields[field] = true
		}
	})

	if isErr {
		//ResponseError(ctx, errStr)
		if isLike {
			// may vary (do not edit/remove this comment)
			ResponseJsonString(ctx, globals.EmptyAccountsString)
		} else {
			Response400(ctx)
		}
		return
	}

	fieldKeys := make([]string, 0, len(fields))
	for k := range fields {
		fieldKeys = append(fieldKeys, k)
	}
	//fieldKeys = append(fieldKeys, "InterestIds")

	query.Select(fieldKeys...)
	query.Sort("id", true)
	//query.Join(db.ReindexerDb.Query(db.InterestsNamespace), "name").On("InterestIds", reindexer.SET, "id")

	iterator := query.ExecToJson()

	str, err := iterator.FetchAll()
	if err != nil {
		//ResponseError(ctx, err.Error())
		Response400(ctx)
		return
	}

	ResponseJsonString(ctx, str)
}

func whereBuilder(key string, value string, query *reindexer.Query) (field string, isErr, isLike bool) {
	switch key {
	case "sex", "sex_eq":
		field = "sex"
		switch value {
		case "f", "m":
			query.WhereString("sex", reindexer.EQ, value)
		default:
			query.Where("sex", reindexer.EQ, nil)
		}
		break
	case "email_domain":
		field = "email"
		query.WhereString("email_domain", reindexer.EQ, value)
	case "email_lt":
		field = "email"
		query.WhereString("email", reindexer.LT, value)
	case "email_gt":
		field = "email"
		query.WhereString("email", reindexer.GT, value)
	case "status", "status_eq":
		field = "status"
		query.WhereString("status", reindexer.EQ, value)
	case "status_neq":
		field = "status"
		query.Not().WhereString("status", reindexer.EQ, value)
	case "fname", "fname_eq":
		field = "fname"
		query.WhereString("fname", reindexer.EQ, value)
	case "fname_any":
		field = "fname"
		query.WhereString("fname", reindexer.SET, strings.Split(value, ",")...)
	case "fname_null":
		field = "fname"
		nullVal, err := strconv.Atoi(value)
		if err != nil {
			isErr = true
			//errStr = "error converting fname \"" + value + "\" to number"
			break
		}

		if nullVal == 1 {
			query.WhereString("fname", reindexer.EQ, "")
		} else if nullVal == 0 {
			query.Not().WhereString("fname", reindexer.EQ, "")
		} else {
			isErr = true
			//errStr = "fname nullVal is " + strconv.Itoa(nullVal)
			break
		}
	case "sname", "sname_eq":
		field = "sname"
		query.WhereString("sname", reindexer.EQ, value)
	case "sname_starts":
		field = "sname"
		query.WhereString("sname", reindexer.GE, value)
		query.WhereString("sname", reindexer.LT, globals.IncrementString(value))
	case "sname_null":
		field = "sname"
		nullVal, err := strconv.Atoi(value)
		if err != nil {
			isErr = true
			//errStr = "error converting sname \"" + value + "\" to number"
			break
		}

		if nullVal == 1 {
			query.WhereBool("sname_bool", reindexer.EQ, false)
		} else if nullVal == 0 {
			query.WhereBool("sname_bool", reindexer.EQ, true)
		} else {
			isErr = true
			//errStr = "sname nullVal is " + strconv.Itoa(nullVal)
			break
		}
	case "phone_code":
		field = "phone"
		code, err := strconv.Atoi(value)
		if err == nil {
			query.WhereInt("phone_code", reindexer.EQ, code)
		} else {
			isErr = true
			//errStr = "error converting phone_code \"" + value + "\" to number"
		}
	case "phone_null":
		field = "phone"

		nullVal, err := strconv.Atoi(value)
		if err != nil {
			isErr = true
			//errStr = "error converting phone_null \"" + value + "\" to number"
			break
		}

		if nullVal == 1 {
			query.WhereInt("phone_code", reindexer.EQ, 0)
		} else if nullVal == 0 {
			query.Not().WhereInt("phone_code", reindexer.EQ, 0)
		} else {
			isErr = true
			//errStr = "phone nullVal is " + strconv.Itoa(nullVal)
			break
		}
	case "country", "country_eq":
		field = "country"
		query.WhereString("country", reindexer.EQ, value)
	case "country_null":
		field = "country"
		nullVal, err := strconv.Atoi(value)
		if err != nil {
			isErr = true
			//errStr = "error converting country_null \"" + value + "\" to number"
			break
		}

		if nullVal == 1 {
			query.WhereString("country", reindexer.EQ, "")
		} else if nullVal == 0 {
			query.Not().WhereString("country", reindexer.EQ, "")
		} else {
			isErr = true
			//errStr = "country_null is " + strconv.Itoa(nullVal)
			break
		}
	case "city", "city_eq":
		field = "city"
		query.WhereString("city", reindexer.EQ, value)
	case "city_any":
		field = "city"
		query.WhereString("city", reindexer.SET, strings.Split(value, ",")...)
	case "city_null":
		field = "city"
		nullVal, err := strconv.Atoi(value)
		if err != nil {
			isErr = true
			//errStr = "error converting city_null \"" + value + "\" to number"
			break
		}

		if nullVal == 1 {
			query.WhereString("city", reindexer.EQ, "")
		} else if nullVal == 0 {
			query.Not().WhereString("city", reindexer.EQ, "")
		} else {
			isErr = true
			//errStr = "city_null is " + strconv.Itoa(nullVal)
			break
		}
	case "birth_lt":
		field = "birth"
		query.Where("birth", reindexer.LT, value)
	case "birth_gt":
		field = "birth"
		query.Where("birth", reindexer.GT, value)
	case "birth", "birth_year":
		field = "birth"
		query.Where("birth_year", reindexer.EQ, value)
	case "joined", "joined_year":
		field = "joined"
		query.Where("joined_year", reindexer.EQ, value)
		// TODO
	case "interests_contains":
		interestsList := strings.Split(value, ",")
		for i := 0; i < len(interestsList); i++ {
			query.WhereString("Interests", reindexer.SET, interestsList[i])
		}
	case "interests_any":
		interestsList := strings.Split(value, ",")
		query.WhereString("Interests", reindexer.SET, interestsList...)
	case "likes_contains":
		isLike = true
		isErr = true
	/*case "likes_contains":
	likesList := strings.Split(value, ",")
	for i := 0; i < len(likesList); i++ {
		likeUser, err := strconv.Atoi(likesList[i])
		if err != nil {
			errStr = "error likes_contains \"" + likesList[i] + "\" to number"
			break
		}
		query.WhereInt("like_ids", reindexer.SET, likeUser)
	}*/
	case "premium_now":
		field = "premium"
		premiumVal, err := strconv.Atoi(value)
		if err != nil {
			isErr = true
			//errStr = "error converting premium_null \"" + value + "\" to number"
			break
		}

		if premiumVal == 1 {
			query.WhereBool("premium_now", reindexer.EQ, true)
		} else if premiumVal == 0 {
			query.WhereBool("premium_now", reindexer.EQ, false)
		} else {
			isErr = true
			//errStr = "premium_null is " + strconv.Itoa(premiumVal)
			break
		}
	case "premium_null":
		field = "premium"
		nullVal, err := strconv.Atoi(value)
		if err != nil {
			isErr = true
			//errStr = "error converting premium_null \"" + value + "\" to number"
			break
		}

		if nullVal == 1 {
			query.WhereBool("premium_existed", reindexer.EQ, false)
		} else if nullVal == 0 {
			query.WhereBool("premium_existed", reindexer.EQ, true)
		} else {
			isErr = true
			//errStr = "premium_null is " + strconv.Itoa(nullVal)
			break
		}
	case "limit":
		limit, err := strconv.Atoi(value)
		if err != nil {
			isErr = true
			//errStr = "error converting limit \"" + value + "\" to number"
			break
		}
		query.Limit(limit)
	case "query_id":
	default:
		isErr = true
		//errStr = "unknown query key  \"" + key + "\""
	}

	return
}
