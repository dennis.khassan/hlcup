package controller

import (
	"github.com/restream/reindexer"
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"unsafe"
)

func LikesAction(ctx *fasthttp.RequestCtx) {
	//atomic.AddUint64(&globals.CntLikes, 1)

	likesRaw := &entity.NewLikesRaw{}
	err := likesRaw.UnmarshalJSON(ctx.Request.Body())
	if err != nil {
		//ResponseError(ctx, "wrong json")
		Response400(ctx)
		return
	}

	uniqueIds := make([]int32, 0, len(likesRaw.Likes)*2)
	uniqueIdsHelpMap := map[int32]bool{}
	//likesMap := map[int32][]entity.LikeRaw{}
	for _, like := range likesRaw.Likes {
		if _, ok := uniqueIdsHelpMap[like.Likee]; !ok {
			uniqueIdsHelpMap[like.Likee] = true
			uniqueIds = append(uniqueIds, like.Likee)
		}
		if _, ok := uniqueIdsHelpMap[like.Liker]; !ok {
			uniqueIdsHelpMap[like.Liker] = true
			uniqueIds = append(uniqueIds, like.Liker)
		}
		//likeRaw := entity.LikeRaw{
		//	Id:        like.Likee,
		//	Timestamp: like.Timestamp,
		//}
		//if val, ok := likesMap[like.Liker]; ok {
		//	likesMap[like.Liker] = append(val, likeRaw)
		//} else {
		//	likesMap[like.Liker] = []entity.LikeRaw{likeRaw}
		//}
	}
	uniqueIdsHelpMap = nil
	iterator := globals.ReindexerDb.Query(globals.AccountsNamespace).
		WhereInt32("id", reindexer.SET, *(*[]int32)(unsafe.Pointer(&uniqueIds))...).
		MustExec().AllowUnsafe(true)

	if iterator.Count() != len(uniqueIds) {
		iterator.Close()
		//ResponseError(ctx, "some accounts not exist")
		Response400(ctx)
		return
	}
	iterator.Close()

	/*for liker, likes := range likesMap {
		// append like
		accountPtr, err := entity.GetAccountById(liker)
		if err != nil {
			panic(err)
		}
		account := *accountPtr
		entity.AppendLikes(&account.LikeIds, likes)
		isUpdated, err := globals.ReindexerDb.Update(globals.AccountsNamespace, account)
		if err != nil {
			panic(err)
		}
		if isUpdated == 0 {
			panic(err)
		}
	}*/

	/*for liker, likes := range likesMap {
		// append like
		likerBytes := entity.GetUintBytes(liker)
		entity.StartWriteExtra(liker)
		extra := entity.GetExtra(likerBytes)
		entity.AppendLikes(&extra, likes)
		entity.PutExtra(likerBytes, &extra)
		entity.FinishWriteExtra(liker)

		// append likedBy
		entity.AppendLikedBy(liker, &extra)
	}*/

	ctx.Response.SetStatusCode(202)
}
