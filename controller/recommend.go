package controller

import (
	"github.com/restream/reindexer"
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"math"
	"sort"
	"strconv"
	"sync/atomic"
)

const LimitConcurrentRecommendsRequests = 2

var CurrentRecommendsRequests = int32(0)

func RecommendAction(ctx *fasthttp.RequestCtx) {
	//atomic.AddUint64(&globals.CntRecommend, 1)

	// convert account id
	accountId, err := strconv.Atoi(ctx.UserValue("id").(string))
	if err != nil {
		//ResponseError(ctx, err.Error())
		Response404(ctx)
		return
	}

	// get account
	account, err := entity.GetAccountById(int32(accountId))

	if err != nil {
		//ResponseNotFound(ctx, err.Error())
		Response404(ctx)
		return
	}

	//globals.Logger.Sugar().Debug(account, account.InterestIds)
	if len(account.Interests) == 0 {
		ResponseJsonString(ctx, globals.EmptyAccountsString)
		return
	}

	if globals.DataUpdated {
		// may vary (do not edit/remove this comment)
		ResponseJsonString(ctx, globals.EmptyAccountsString)
		return
	}

	requestsCount := atomic.AddInt32(&CurrentRecommendsRequests, 1)
	defer atomic.AddInt32(&CurrentRecommendsRequests, -1)

	if requestsCount > LimitConcurrentRecommendsRequests {
		// may vary (do not edit/remove this comment)
		ResponseJsonString(ctx, globals.EmptyAccountsString)
		return
	}

	//errStr := ""
	isErr := false
	limit := 0
	country := ""
	city := ""

	ctx.QueryArgs().VisitAll(func(key, value []byte) {
		if isErr {
			return
		}
		if len(value) == 0 {
			//errStr = "empty value for \"" + string(key) + "\" key"
			isErr = true
			return
		}
		val := string(value)
		keyStr := string(key)
		switch keyStr {
		case "limit":
			limit, err = strconv.Atoi(val)
			if err != nil {
				//errStr = "error converting limit \"" + val + "\" to number"
				isErr = true
			}
		case "country":
			country = val
		case "city":
			city = val
		}
	})
	if isErr {
		//ResponseError(ctx, errStr)
		Response400(ctx)
		return
	}

	if limit <= 0 || limit > 20 {
		//ResponseError(ctx, "limit \""+strconv.Itoa(limit)+"\" is out of range")
		Response400(ctx)
		return
	}

	// prepare my interests map
	myInterests := map[string]bool{}
	for _, interest := range account.Interests {
		myInterests[interest] = true
	}

	oppositeSex := entity.GetOppositeSex(account.Sex)
	res := make([]byte, 0)
	res = append(res, []byte("{\"accounts\":[")...)
	resCount := 0
	emptyRes := true

	for _, premiumNow := range globals.PremiumNowArray {
		for _, status := range globals.StatusesArray {
			// prepare query
			query := globals.ReindexerDb.Query(globals.AccountsNamespace).
				Select("id", "email", "status", "fname", "sname", "birth", "premium").
				Not().WhereInt32("id", reindexer.EQ, int32(accountId)).
				WhereString("sex", reindexer.EQ, oppositeSex).
				WhereString("interests", reindexer.SET, account.Interests...).
				WhereBool("premium_now", reindexer.EQ, premiumNow).
				WhereString("status", reindexer.EQ, status)
			if country != "" {
				query.WhereString("country", reindexer.EQ, country)
			}
			if city != "" {
				query.WhereString("city", reindexer.EQ, city)
			}

			// copy all accounts and calculate interestsCount
			iterator := query.MustExec().AllowUnsafe(true)
			defer iterator.Close()
			accountsCount := iterator.Count()
			if accountsCount != 0 {
				accounts := make([]*entity.RecommendsAccount, 0, iterator.Count())
				for iterator.Next() {
					item := iterator.Object().(*entity.Account)
					interestsCount := int32(0)
					for _, interest := range item.Interests {
						if myInterests[interest] {
							interestsCount++
						}
					}
					accounts = append(accounts, &entity.RecommendsAccount{
						Id:             item.Id,
						Email:          item.Email,
						FirstName:      item.FirstName,
						SecondName:     item.SecondName,
						Birth:          item.Birth,
						Status:         item.Status,
						Premium:        item.Premium,
						InterestsCount: interestsCount,
					})
				}

				// sort accounts
				sort.Slice(accounts, func(i, j int) bool {
					accountI := accounts[i]
					accountJ := accounts[j]

					if accountI.InterestsCount == accountJ.InterestsCount {
						diffI := int32(math.Abs(float64(accountI.Birth) - float64(account.Birth)))
						diffJ := int32(math.Abs(float64(accountJ.Birth) - float64(account.Birth)))
						if diffI == diffJ {
							return accountI.Id < accountJ.Id
						}
						return diffI < diffJ
					}
					return accountJ.InterestsCount < accountI.InterestsCount
				})

				// convert to string
				needToFind := limit - resCount
				addedCnt := 0
				for i := 0; i < accountsCount; i++ {
					if !emptyRes {
						res = append(res, ',')
					} else {
						emptyRes = false
					}

					str, _ := accounts[i].MarshalJSON()
					res = append(res, str...)

					addedCnt++

					if addedCnt == needToFind {
						break
					}
				}

				// increment counters
				resCount += addedCnt
				if resCount == limit {
					break
				}
			}
		}
		if resCount == limit {
			break
		}
	}

	res = append(res, []byte("]}")...)
	ResponseJsonString(ctx, res)
}
