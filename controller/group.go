package controller

import (
	"fmt"
	"github.com/restream/reindexer"
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"gitlab.com/dennis.khassan/hlcup/globals"
	"sort"
	"strings"
)

type ResponseGroup struct {
	Groups []Group `json:"groups"`
}

type Group struct {
	Country  string `json:"country,omitempty"`
	City     string `json:"city,omitempty"`
	Sex      string `json:"sex,omitempty"`
	Status   string `json:"status,omitempty"`
	Interest string `json:"interests,omitempty"`
	Count    int    `json:"count"`
}

func GroupAction(ctx *fasthttp.RequestCtx) {
	//atomic.AddUint64(&globals.CntGroup, 1)

	var keys []string
	hasInterest := false
	//err := ""
	isErr := false

	queryArgs := ctx.QueryArgs()
	keys = strings.Split(string(queryArgs.Peek("keys")), ",")
	if len(keys) == 0 {
		//ResponseError(ctx, "keys not supplied")
		Response400(ctx)
		return
	}

	orderDesc := string(queryArgs.Peek("order")) == "-1"

	limit := queryArgs.GetUintOrZero("limit")
	if limit <= 0 || limit > 50 {
		//ResponseError(ctx, "limit \""+strconv.Itoa(limit)+"\" is out of range")
		Response400(ctx)
		return
	}

	for _, key := range keys {
		if key != "sex" && key != "status" && key != "interests" && key != "country" && key != "city" {
			//err = "wrong parameter"
			isErr = true
			break
		}

		if key == "interests" {
			hasInterest = true
			break
		}
	}
	if isErr {
		//ResponseError(ctx, err)
		Response400(ctx)
		return
	}

	if globals.DataUpdated {
		// may vary (do not edit/remove this comment)
		ResponseJsonString(ctx, globals.EmptyGroupsString)
		return
	}

	//TODO removed for testing
	if hasInterest || len(keys) > 1 {
		// may vary (do not edit/remove this comment)
		ResponseJsonString(ctx, globals.EmptyGroupsString)
		return
	}

	queryArgs.Del("order")
	queryArgs.Del("limit")
	queryArgs.Del("keys")
	queryArgs.Del("query_id")

	queryArgsMap := map[string]string{}
	queryArgs.VisitAll(func(key, value []byte) {
		keyString := string(key)
		valueString := string(value)
		queryArgsMap[keyString] = valueString
	})

	var response ResponseGroup
	var err, like bool
	response.Groups, err, like = GetGroupedData(globals.ReindexerDb, keys, hasInterest, queryArgsMap, orderDesc, limit)
	if err {
		//ResponseError(ctx, err)
		if like {
			// may vary (do not edit/remove this comment)
			ResponseJsonString(ctx, globals.EmptyGroupsString)
		} else {
			Response400(ctx)
		}
		return
	}

	responseJson, _ := response.MarshalJSON()
	ctx.Response.SetStatusCode(200)
	ResponseJsonString(ctx, responseJson)
}

func GetGroupedData(db *reindexer.Reindexer, keys []string, hasInterest bool, queryArgsMap map[string]string, orderDesc bool, limit int) (facets []Group, isErr, isLike bool) {
	query := db.Query("accounts")

	groupingForOneKeyNotInterest := false
	if len(keys) == 1 && !hasInterest {
		groupingForOneKeyNotInterest = true
	}

	// should exists BEFORE Where, otherwise it will not work
	if groupingForOneKeyNotInterest {
		query = query.Aggregate(keys[0], reindexer.AggFacet)
	}

	for key, value := range queryArgsMap {
		_, isErr, isLike = whereBuilder(key, value, query)
		if isErr {
			break
		}
	}
	if isErr {
		return
	}

	iterator := query.MustExec().AllowUnsafe(true)
	defer iterator.Close()

	facets = iterateData(*iterator, keys, groupingForOneKeyNotInterest)

	facets = sortResultsCount(facets, orderDesc)

	facets = limitResults(limit, facets)

	return facets, false, false
}

func iterateData(iterator reindexer.Iterator, keys []string, groupingForOneKeyNotInterest bool) (facets []Group) {
	facets = []Group{}

	if groupingForOneKeyNotInterest {
		aggResAll := iterator.AggResults()
		if len(aggResAll) == 0 {
			return
		}

		aggRes := aggResAll[0]

		for _, facet := range aggRes.Facets {
			countryValue := ""
			cityValue := ""
			sexValue := ""
			statusValue := ""
			interestValue := ""

			if facet.Value == "" {
				continue
			}

			switch keys[0] {
			case "country":
				countryValue = facet.Value
			case "city":
				cityValue = facet.Value
			case "sex":
				sexValue = facet.Value
			case "status":
				statusValue = facet.Value
			}

			facets = appendFacet(facets, countryValue, cityValue, sexValue, statusValue, interestValue, facet.Count)
		}

	} else {
		results := improvedIterator(&iterator, keys)

		for key, count := range results {
			keyValues := strings.Split(key, "_")

			countryValue := ""
			cityValue := ""
			sexValue := ""
			statusValue := ""
			interestValue := ""

			skipFacet := false

			for i, v := range keys {
				if keyValues[i] == "" {
					skipFacet = true
					break
				}

				switch v {
				case "country":
					countryValue = keyValues[i]
				case "city":
					cityValue = keyValues[i]
				case "sex":
					sexValue = keyValues[i]
				case "status":
					statusValue = keyValues[i]
				case "interests":
					interestValue = keyValues[i]
				}
			}

			if !skipFacet {
				facets = appendFacet(facets, countryValue, cityValue, sexValue, statusValue, interestValue, count)
			}
		}
	}

	return
}

//func fixVariableNames(facets []Group, interests map[string]string) {
//	for i, _ := range facets {
//		if facets[i].Interest != "" {
//			facets[i].Interest = interests[facets[i].Interest]
//		}
//	}
//}

func sortResultsCount(facets []Group, orderDesc bool) []Group {
	if len(facets) < 2 {
		return facets
	}

	sort.SliceStable(facets, func(i, j int) bool {
		if orderDesc {
			if facets[i].Count != facets[j].Count {
				return facets[i].Count > facets[j].Count
			}

			if facets[i].Country != facets[j].Country {
				return facets[i].Country > facets[j].Country
			}

			if facets[i].City != facets[j].City {
				return facets[i].City > facets[j].City
			}

			if facets[i].Status != facets[j].Status {
				return facets[i].Status > facets[j].Status
			}

			if facets[i].Sex != facets[j].Sex {
				return facets[i].Sex > facets[j].Sex
			}

			return facets[i].Interest > facets[j].Interest

		} else {
			if facets[i].Count != facets[j].Count {
				return facets[i].Count < facets[j].Count
			}

			if facets[i].Country != facets[j].Country {
				return facets[i].Country < facets[j].Country
			}

			if facets[i].City != facets[j].City {
				return facets[i].City < facets[j].City
			}

			if facets[i].Status != facets[j].Status {
				return facets[i].Status < facets[j].Status
			}

			if facets[i].Sex != facets[j].Sex {
				return facets[i].Sex < facets[j].Sex
			}

			return facets[i].Interest < facets[j].Interest
		}
	})

	return facets
}

func limitResults(limit int, facets []Group) []Group {
	if len(facets) < limit {
		limit = len(facets)
	}
	return facets[0:limit]
}

func appendFacet(facets []Group, countryValue string, cityValue string, sexValue string, statusValue string, interestValue string, count int) []Group {
	facets = append(facets, Group{
		Country:  countryValue,
		City:     cityValue,
		Interest: interestValue,
		Sex:      sexValue,
		Status:   statusValue,
		Count:    count,
	})
	return facets
}

func improvedIterator(iterator *reindexer.Iterator, keys []string) map[string]int {
	results := map[string]int{}
	hasInterest := false

	for iterator.Next() {
		elem := iterator.Object().(*entity.Account)

		var interests []string
		baseKey := ""

		for i, _ := range keys {
			switch keys[i] {
			case "interests":
				hasInterest = true
				interests = getInterests(elem)
			default:
				if i == 0 {
					baseKey = getValue(elem, keys[i])
				} else {
					baseKey += "_" + getValue(elem, keys[i])
				}
			}
		}

		if hasInterest && len(keys) > 1 {
			baseKey = baseKey + "_"
		}

		if len(interests) == 0 {
			incrementKey(results, baseKey)
		} else {
			for i := 0; i < len(interests); i++ {
				newKey := baseKey + fmt.Sprint(interests[i])
				incrementKey(results, newKey)
			}
		}
	}

	return results
}

func incrementKey(results map[string]int, newKey string) {
	if newKey == "" {
		return
	}

	if _, ok := results[newKey]; ok {
		results[newKey]++
	} else {
		results[newKey] = 1
	}
}

func getInterests(account *entity.Account) []string {
	return account.Interests
}

func getValue(account *entity.Account, key string) string {
	switch key {
	case "country":
		return account.Country
	case "city":
		return account.City
	case "status":
		return account.Status
	case "sex":
		return account.Sex
	}
	return ""
}
