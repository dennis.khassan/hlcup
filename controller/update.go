package controller

import (
	"github.com/restream/reindexer"
	"github.com/spf13/cast"
	"github.com/valyala/fasthttp"
	"gitlab.com/dennis.khassan/hlcup/entity"
	"gitlab.com/dennis.khassan/hlcup/globals"
)

//type updateAccountMap struct {
//	mu sync.Mutex
//
//	acc map[uint32]*sync.Mutex
//}
//
//func (upd *updateAccountMap) Lock(id uint32) *sync.Mutex {
//	var mut *sync.Mutex
//	var ok bool
//	upd.mu.Lock()
//	if mut, ok = upd.acc[id]; !ok {
//		mut = &sync.Mutex{}
//		upd.acc[id] = mut
//	}
//	mut.Lock()
//	upd.mu.Unlock()
//	return mut
//}
//
//func (upd *updateAccountMap) Release(id uint32, mut *sync.Mutex) {
//	mut.Unlock()
//	upd.mu.Lock()
//	if **(**int32)(unsafe.Pointer(&mut)) == 0 {
//		delete(upd.acc, id)
//	}
//	upd.mu.Unlock()
//}
//
//var upaccMap = &updateAccountMap{
//	acc: make(map[uint32]*sync.Mutex),
//}

func UpdateAction(ctx *fasthttp.RequestCtx) {
	//atomic.AddUint64(&globals.CntUpdate, 1)

	idInterface := ctx.UserValue("id")
	idString := idInterface.(string)
	id, err := cast.ToUint32E(idString)
	if err != nil {
		//ResponseNotFound(ctx, "error converting id to int")
		Response404(ctx)
		return
	}
	requestJson := ctx.Request.Body()
	if len(requestJson) < 2 {
		//ResponseError(ctx, "empty JSON body")
		Response400(ctx)
		return
	}

	//mutex := upaccMap.Lock(id)

	accEntity, err := entity.GetAccountById(int32(id))
	if err != nil {
		//ResponseNotFound(ctx, err.Error())
		Response404(ctx)
		//upaccMap.Release(id, mutex)

		return
	}

	accountRaw := accEntity.GetAccountRaw()

	err = accountRaw.UnmarshalJSON(requestJson)
	if err != nil {
		//ResponseError(ctx, err.Error())
		Response400(ctx)
		//upaccMap.Release(id, mutex)

		return
	}

	if !accountRaw.Validate() {
		//ResponseError(ctx, "can't validate account")
		Response400(ctx)
		//upaccMap.Release(id, mutex)

		return
	}
	if accountRaw.Sex != accEntity.Sex {
		//ResponseError(ctx, "can't change sex")
		Response400(ctx)
		//upaccMap.Release(id, mutex)

		return
	}

	accountRaw.Id = id

	// Проверяем, есть ли уже такой email и телефон
	if accountRaw.Email != "" && accountRaw.Email != accEntity.Email {
		iterator := globals.ReindexerDb.Query(globals.AccountsNamespace).
			Select("email").
			Where("email", reindexer.EQ, accountRaw.Email).
			Limit(1).
			MustExec().
			AllowUnsafe(true)
		iterator.Close()

		if iterator.Count() > 0 {
			//ResponseError(ctx, "email already exists")
			Response400(ctx)

			//upaccMap.Release(id, mutex)
			return
		}
		iterator.Close()
	}
	//if accountRaw.Phone != "" && accountRaw.Phone != accEntity.Phone {
	//	_, found := globals.ReindexerDb.Query(globals.AccountsNamespace).
	//		Select("phone").
	//		Where("phone", reindexer.EQ, accountRaw.Phone).
	//		Get()
	//	if found {
	//		//ResponseError(ctx, "phone already exists")
	//		Response400(ctx)
	//		upaccMap.Release(id, mutex)
	//		return
	//	}
	//}

	go func(accRaw *entity.AccountRaw) {
		account := accRaw.FetchDataEntities(false)

		isUpdated, err := globals.ReindexerDb.Update(globals.AccountsNamespace, account)
		//upaccMap.Release(accRaw.Id, mut)

		if err != nil {
			panic(err)
		}
		if isUpdated == 0 {
			panic(err)
		}
	}(accountRaw)

	//for _, interest := range interests {
	//	err := db.ReindexerDb.Upsert(db.InterestsNamespace, interest)
	//	if err != nil {
	//		globals.Logger.Sugar().Error(err)
	//	}
	//}

	//accFinal, found := globals.ReindexerDb.Query(globals.AccountsNamespace).
	//	Where("id", reindexer.EQ, id).
	//	Get()
	//
	//if !found {
	//	ctx.Response.SetStatusCode(202)
	//	ctx.Response.SetBody([]byte("Error"))
	//}
	//accFinalT := accFinal.(*entity.Account)
	//body, _ := json.Marshal(accFinalT)
	//ctx.Response.SetBody(body)

	ctx.Response.SetStatusCode(202)
	ctx.Response.SetBody(globals.EmptyJsonString)
}
